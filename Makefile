	# mpiicpx main.cpp -g -O2 -Wall -Wextra -fno-omit-frame-pointer -fstack-protector -trace -o main
dynamic:
	mpiicpx main_dynamicwindow_1epoch_atomic.cpp -g -O2 -Wall -Wextra -fno-omit-frame-pointer -fstack-protector -trace -o main
	# mpiicpx main_dynamicwindow_Nepoch_atomic.cpp -g -O2 -Wall -Wextra -fno-omit-frame-pointer -fstack-protector -trace -o main
	#
fixed:
	mpiicpx main_fixedwindow_1epoch_atomic.cpp -g -O2 -Wall -Wextra -fno-omit-frame-pointer -fstack-protector -trace -o main

shared:
	mpiicpx main_sharedwindow_1epoch_atomic.cpp -g -O2 -Wall -Wextra -fno-omit-frame-pointer -fstack-protector -trace -o main	

clean:
	rm main main.stf main.stf.* main.prot
