#include "mpi.h"
#include "heat_serial.h"
#include <vector>
#include <iostream>
#include <cstddef>

// int mpi_lock=MPI_LOCK_EXCLUSIVE;
int mpi_lock=MPI_LOCK_SHARED;
int mpi_mode=MPI_MODE_NOCHECK;
// int mpi_mode=0;

//----------------------------------------------------------------------------- 
// Pointer to a memory location in a mpi-rank
//----------------------------------------------------------------------------- 
struct mpiPtr{ 
  int rank = -1;        
  MPI_Aint disp = -1;
};

// Equivalent to a null pointer
mpiPtr target_null = {-1, -1};

//----------------------------------------------------------------------------- 
// Node for a linked list
//----------------------------------------------------------------------------- 

struct Node {
  int id=-1;
  mpiPtr next={-1,-1};
};

mpiPtr GetMpiPtr(int myrank, int index) {
  mpiPtr target_node;
  target_node.rank = myrank;

  MPI_Aint disp = MPI_Aint( index*sizeof(Node) );
  target_node.disp = disp;

  return target_node;
}

//-------------------------------------------------------------------------------- 
// Main
//-------------------------------------------------------------------------------- 
int main(int argc, char* argv[]) {

  int rank, size;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  double t0 = MPI_Wtime();

  if (rank == 0){
    printf("=============\n");
    printf("Shared window\n");
    printf("=============\n");
  }

  int n_nodes = 5;
  if (rank==0) 
    n_nodes++;

  // Linked list
  char* mynodes; // nodes allocated by this rank

  // Create fixed size window
  MPI_Win win = MPI_WIN_NULL;

  MPI_Win_allocate_shared(
      sizeof(Node)*n_nodes, // MPI_Aint size,
      sizeof(char),         // int disp_unit,
      MPI_INFO_NULL,        // MPI_Info info,
      MPI_COMM_WORLD,       // MPI_Comm comm,
      &mynodes,             // void *baseptr,
      &win                  // MPI_Win *win)
    );

  // Assign UID
  Node* nodes = (Node*)mynodes;
  for (int i=0; i<n_nodes; i++)
  {
    nodes[i].id = rank*100+i;
    nodes[i].next = target_null;        
  }

  mpiPtr head, tail;                  // head and tail of linked list

  int i_node=0;
  // Rank 0, creates the first node of the list and broadcast the disp
  if (rank == 0) {
    head = GetMpiPtr(rank, i_node); 
    printf("[Rank %d] dummy node %d/%d appended successfully to the list\n", rank, i_node+1, n_nodes);
    i_node++;
  }
  head.rank = 0;
  MPI_Bcast(&head.disp, 1, MPI_AINT, 0, MPI_COMM_WORLD);

  // Update tail
  tail = head;

  // Lock window for shared access 
  MPI_Win_lock_all(0, win);

  printf("[Rank %d] Must append %d to the linked list\n", rank, n_nodes);
  for (int inode=i_node; inode<n_nodes; inode++) {

    int success;
    mpiPtr node = GetMpiPtr(rank, inode);

    // all process try to append n_nodes to the list 
    do {
      mpiPtr newtail = target_null;

      // target points to tail.next.rank
      mpiPtr target;
      target.rank = tail.rank;
      target.disp = MPI_Aint_add(tail.disp, offsetof(Node, next.rank));

      // MPI_Win_lock(mpi_lock, target.rank, mpi_mode, win);
      MPI_Compare_and_swap(
          &node.rank,        // const void   *origin_addr,
          &target_null.rank, // const void   *compare_addr,
          &newtail.rank,     // void         *result_addr,
          MPI_INT,           // MPI_Datatype datatype,
          target.rank,       // int          target_rank,
          target.disp,       // MPI_Aint     target_disp,
          win                // MPI_Win      win
          );
      // MPI_Win_unlock(target.rank, win);
      MPI_Win_flush(target.rank, win);

      success = (newtail.rank == target_null.rank);

      if (success) {
        // Rank wrote its rank id in the tail, now update the disp

        // target points to tail.next.disp
        mpiPtr target;
        target.rank = tail.rank;
        target.disp = MPI_Aint_add(tail.disp, offsetof(Node, next.disp));

        // Atomic put
        // MPI_Win_lock(mpi_lock, target.rank, mpi_mode, win);
        MPI_Accumulate(
            &node.disp,  // const void     *origin_addr,
            1,           // int            origin_count,
            MPI_AINT,    // MPI_Datatype   origin_datatype,
            target.rank, // int            target_rank,
            target.disp, // MPI_Aint       target_disp,
            1,           // int            target_count,
            MPI_AINT,    // MPI_Datatype   target_datatype,
            MPI_REPLACE, // MPI_Op         op,
            win);        // MPI_Win        win
        // MPI_Win_unlock(target.rank, win);
        MPI_Win_flush(target.rank, win);
        tail = node;

        printf("[Rank %d] node %d/%d appended successfully to the list. Start computations...\n", rank, inode+1, n_nodes);

        // Do some heavy local computation
        // -----------------------------
        solve_2D_heat_equation();
        // -----------------------------

        printf("[Rank %d] computations after adding node %d/%d done. Ready to add more nodes\n", rank, inode+1, n_nodes);

      } else {
        // Tail pointer is stale, fetch the disp (update the tail)
        // May take multiple tries if it is being updated.

        do {
          // target points to tail.next.disp
          mpiPtr target;
          target.rank = tail.rank;
          target.disp = MPI_Aint_add(tail.disp, offsetof(Node, next.disp));

          // Atomic get
          // MPI_Win_lock(mpi_lock, target.rank, mpi_mode, win);
          MPI_Fetch_and_op(NULL, // origin_addr     (ignore)
              &newtail.disp,     // result_addr
              MPI_AINT,          // datatype
              target.rank,       // target_rank
              target.disp,       // target disp
              MPI_NO_OP,         // op
              win);
          // MPI_Win_unlock(target.rank, win);
          MPI_Win_flush(target.rank, win);
        } while (newtail.disp == target_null.disp);

        // Update tail
        tail = newtail;
      }
    } while (!success);
  }

  printf("[Rank %d] No more nodes to add\n", rank);

  // Clean up
  MPI_Barrier(MPI_COMM_WORLD);

  // Print nodes
  if(rank==0){
    printf("[Rank %d]: print list \n", rank);
    printf("======================================================= \n");

    // int offset=0;
    // for (int i=0; i<21; i++) {
    //   Node* node = (Node*)(mynodes+offset);
    //   printf("-- [Rank %d]: %d, next {%d, %ld}\n",rank,  node->id, node->next.rank, node->next.disp);
    //   offset+=sizeof(Node);
    // }

    Node node;
    mpiPtr newtail = head;
    int counter=0;
    do {
      // target points to tail.next.disp
      mpiPtr target;
      target.rank = newtail.rank;
      target.disp = MPI_Aint_add(newtail.disp, offsetof(Node, id));

      // Get
      MPI_Get(
          &node,        // origin_addr
          sizeof(Node), // origin_count
          MPI_CHAR,     // origin_datatype
          target.rank,  // target_rank
          target.disp,  // target disp
          sizeof(Node), // target_count
          MPI_CHAR,     // target_datatype
          win);
      MPI_Win_flush(target.rank, win);
      printf("-- [Rank %d]: %d \n", rank, node.id);

      newtail = node.next;
      counter++;
    } while (newtail.rank != target_null.rank);
    printf("[Rank %d]: %d nodes in the list \n", rank, counter);
  }

  MPI_Barrier(MPI_COMM_WORLD);

  // Clean up
  MPI_Win_unlock_all(win);
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Win_free(&win);

  std::cout << "Execution time: " << MPI_Wtime()-t0 << " sec.\n";
  MPI_Finalize();
}
