#include "mpi.h"
#include "heat_serial.h"
#include <vector>
#include <iostream>
#include <cstddef>

int mpi_lock=MPI_LOCK_SHARED;
// int mpi_lock=MPI_LOCK_EXCLUSIVE;
int mpi_mode=MPI_MODE_NOCHECK;
// int mpi_mode=0;

//----------------------------------------------------------------------------- 
// Pointer to a memory location in a mpi-rank
//----------------------------------------------------------------------------- 
struct mpiPtr{ 
  int rank = -1;        
  MPI_Aint address = (MPI_Aint)MPI_BOTTOM;
};

// Equivalent to a null pointer
mpiPtr target_null = {-1, (MPI_Aint)MPI_BOTTOM};

//----------------------------------------------------------------------------- 
// Node for a linked list
//----------------------------------------------------------------------------- 

struct Node {
  int id = -1;
  mpiPtr next = target_null;
};

// Allocate a node and attach it to the window
// - returns an mpiPtr of the allocated node
mpiPtr AllocateAndAttachNode(int myrank, std::vector<Node*> &mynodes, MPI_Win win) {

  // Allocate node and initialize it to "null"
  Node* node;
  MPI_Alloc_mem(sizeof(Node), MPI_INFO_NULL, &node);
  node->id = myrank*100 + mynodes.size();
  node->next = target_null;

  mynodes.push_back(node);
  
  // Attach memory to the window
  MPI_Win_attach(win, node, sizeof(Node));

  // Create a target pointer for the allocated node
  mpiPtr target_node;
  target_node.rank = myrank;
  MPI_Get_address(node, &target_node.address);
  return target_node;
}

//-------------------------------------------------------------------------------- 
// Main
//-------------------------------------------------------------------------------- 
int main(int argc, char* argv[]) {

  int rank, size;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  double t0 = MPI_Wtime();

  int const n_nodes = 5;

  // Linked list
  std::vector<Node*> mynodes; // nodes allocated by this rank
  mpiPtr head, tail;          // head and tail of linked list

  // Create dynamic window
  MPI_Win win = MPI_WIN_NULL;
  MPI_Win_create_dynamic(MPI_INFO_NULL, // MPI_Info
      MPI_COMM_WORLD,                   // MPI_Comm
      &win                              // MPI_Win*
      );

  // Rank 0, creates the first node of the list and broadcast the address
  if (rank == 0) {
    head = AllocateAndAttachNode(rank, mynodes, win); 
  }
  head.rank = 0;
  MPI_Bcast(&head.address, 1, MPI_AINT, 0, MPI_COMM_WORLD);

  // Update tail
  tail = head;

  // Lock window for shared access 
  // MPI_Win_lock_all(0, win);

  printf("[Rank %d] Must append %d to the linked list\n", rank, n_nodes);
  for (int inode = 0; inode < n_nodes; inode++) {
    int success;
    mpiPtr node = AllocateAndAttachNode(rank, mynodes, win);

    // all process try to append n_nodes to the list 
    do {
      mpiPtr newtail = target_null;

      // target points to tail.next.rank
      mpiPtr target;
      target.rank = tail.rank;
      target.address = MPI_Aint_add(tail.address, offsetof(Node, next.rank));

      MPI_Win_lock(mpi_lock, target.rank, mpi_mode, win);
      MPI_Compare_and_swap(
          &node.rank,         // const void   *origin_addr,
          &target_null.rank,  // const void   *compare_addr,
          &newtail.rank,      // void         *result_addr,
          MPI_INT,            // MPI_Datatype datatype,
          target.rank,        // int          target_rank,
          target.address,     // MPI_Aint     target_disp,
          win                 // MPI_Win      win
          );
      MPI_Win_unlock(target.rank, win);

      success = (newtail.rank == target_null.rank);

      if (success) {
        // Rank wrote its rank id in the tail, now update the address

        // target points to tail.next.address
        mpiPtr target;
        target.rank = tail.rank;
        target.address = MPI_Aint_add(tail.address, offsetof(Node, next.address));

        // Atomic put
        MPI_Win_lock(mpi_lock, target.rank, mpi_mode, win);
        MPI_Accumulate(
            &node.address,        // const void     *origin_addr,
            1,                    // int            origin_count,
            MPI_AINT,             // MPI_Datatype   origin_datatype,
            target.rank,          // int            target_rank,
            target.address,       // MPI_Aint       target_disp,
            1,                    // int            target_count,
            MPI_AINT,             // MPI_Datatype   target_datatype,
            MPI_REPLACE,          // MPI_Op         op,
            win);                 // MPI_Win        win
        MPI_Win_unlock(target.rank, win);
        tail = node;

        printf("[Rank %d] node %d/%d appended successfully to the list. Start computations...\n", rank, inode+1, n_nodes);

        // Do some heavy local computation
        // -----------------------------
        solve_2D_heat_equation();
        // -----------------------------

        printf("[Rank %d] computations after adding node %d/%d done. Ready to add more nodes\n", rank, inode+1, n_nodes);

      } else {
        // Tail pointer is stale, fetch the address (update the tail)
        // May take multiple tries if it is being updated.

        do {
          // target points to tail.next.address
          mpiPtr target;
          target.rank = tail.rank;
          target.address = MPI_Aint_add(tail.address, offsetof(Node, next.address));

          MPI_Win_lock(mpi_lock, target.rank, mpi_mode, win);
          // Atomic get
          MPI_Fetch_and_op(NULL, // origin_addr     (ignore)
              &newtail.address,  // result_addr
              MPI_AINT,          // datatype
              target.rank,       // target_rank
              target.address,    // target disp
              MPI_NO_OP,         // op
              win);
          MPI_Win_unlock(target.rank, win);
        } while (newtail.address == target_null.address);

        // Update tail
        tail = newtail;
      }
    } while (!success);
  }

  printf("[Rank %d] No more nodes to add\n", rank);

  MPI_Barrier(MPI_COMM_WORLD);

  // Print nodes
  if(rank==0){
    printf("[Rank %d]: print list \n", rank);
    printf("======================================================= \n");

    Node node;
    mpiPtr newtail = head;
    int counter=0;
    do {
      mpiPtr target;
      target.rank = newtail.rank;
      target.address = MPI_Aint_add(newtail.address, offsetof(Node, id));

      // Get
      MPI_Win_lock(mpi_lock, target.rank, mpi_mode, win);
      MPI_Get(
          &node,          // origin_addr
          sizeof(Node),   // origin_count
          MPI_CHAR,       // origin_datatype
          target.rank,    // target_rank
          target.address, // target address
          sizeof(Node),   // target_count
          MPI_CHAR,       // target_datatype
          win);
      MPI_Win_unlock(target.rank, win);
      printf("-- [Rank %d]: %d \n", rank, node.id);

      newtail = node.next;
      counter++;
    } while (newtail.address != target_null.address);
    printf("[Rank %d]: %d nodes in the list \n", rank, counter);
  }

  MPI_Barrier(MPI_COMM_WORLD);

  // Clean up
  // MPI_Win_unlock_all(win);
  MPI_Barrier(MPI_COMM_WORLD);

  for (auto inode = mynodes.size(); inode-- > 0;) {
    MPI_Win_detach(win, mynodes[inode]);
    MPI_Free_mem(mynodes[inode]);
  }

  MPI_Win_free(&win);

  std::cout << "Execution time: " << MPI_Wtime()-t0 << " sec.\n";
  MPI_Finalize();
}
