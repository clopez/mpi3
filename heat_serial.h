#include <algorithm>
#include <chrono>

// Solution of the 2D heat equation
size_t heat(const int kImax, const int kKmax, const int kItmax, const double kEps) {
  double phi[kImax + 1][kKmax + 1];
  double phin[kImax][kKmax];

  double dx = 1.0 / kKmax;
  double dy = 1.0 / kImax;
  double dx2 = dx * dx;
  double dy2 = dy * dy;
  double dx2i = 1.0 / dx2;
  double dy2i = 1.0 / dy2;
  double dt = std::min(dx2, dy2) / 4.0;
  /* start values 0.d0 */
  for (int k = 0; k < kKmax; k++) {
    for (int i = 1; i < kImax; i++) {
      phi[i][k] = 0.0;
    }
  }
  /* start values 1.d0 */
  for (int i = 0; i <= kImax; i++) {
    phi[i][kKmax] = 1.0;
  }
  /* start values dx */
  phi[0][0] = 0.0;
  phi[kImax][0] = 0.0;
  for (int k = 1; k < kKmax; k++) {
    phi[0][k] = phi[0][k - 1] + dx;
    phi[kImax][k] = phi[kImax][k - 1] + dx;
  }

  auto begin = std::chrono::steady_clock::now();

  /* iteration */
  int it;
  double dphi, dphimax;
  for (it = 1; it <= kItmax; it++) {
    dphimax = 0.;
    for (int k = 1; k < kKmax; k++) {
      for (int i = 1; i < kImax; i++) {
        dphi = (phi[i + 1][k] + phi[i - 1][k] - 2. * phi[i][k]) * dy2i +
          (phi[i][k + 1] + phi[i][k - 1] - 2. * phi[i][k]) * dx2i;
        dphi = dphi * dt;
        dphimax = std::max(dphimax, dphi);
        phin[i][k] = phi[i][k] + dphi;
      }
    }
    /* save values */
    for (int k = 1; k < kKmax; k++) {
      for (int i = 1; i < kImax; i++) {
        phi[i][k] = phin[i][k];
      }
    }
    if (dphimax < kEps) break;
  }
  auto end = std::chrono::steady_clock::now();
  size_t elapsed_time_us = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();
  return elapsed_time_us;
}

void solve_2D_heat_equation() {
  int imax = 20;
  int kmax = 20;
  int itmax = 20000;
  double eps = 1e-8;
  heat(imax, kmax, itmax, eps);
}
